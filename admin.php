<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
    </head>
    <body>
        <?php
            session_start();
            $conn = mysqli_connect('localhost','root','','proyek_imk');

            if(isset($_POST['btnLogout'])){
                header("location:loginUser.php?");
                unset($_SESSION['halamanAdmin']);
            }
            if(isset($_POST['btnUser'])){
                $_SESSION['halamanAdmin'] = 0;
                $_SESSION['user'] = 0;
            }
            if(isset($_POST['btnCourier'])){
                $_SESSION['halamanAdmin'] = 1;
            }
            if(isset($_POST['btnHistory'])){
                $_SESSION['halamanAdmin'] = 2;
            }

            if(isset($_POST['btnSubmitRegis'])){
                $cek = true;
                $nama = $_POST['tbNama'];
                $username = $_POST['tbUsername'];
                $password = $_POST['tbPassword'];
                $alamat = $_POST['tbAlamat'];
                $kodepos = $_POST['tbKodepos'];
                $notelp = $_POST['tbNotelp'];
                $sql = "SELECT * FROM courier";
                $courier = $conn -> query($sql);
                while($row = $courier -> fetch_assoc()){
                    if($row['username']==$username) $cek = false;
                }
                if($cek==true){
                    $sql = "INSERT INTO courier VALUES ('$nama','$username','$password','$alamat','$kodepos','$notelp')";
                    $conn -> query($sql);
                }
                else echo "Username telah terpakai";
            }

            if(isset($_POST['btnDelete'])){
                $temp = $_POST['hiddenCourier'];
                $sql = "DELETE FROM courier WHERE username = '$temp'";
                $conn -> query($sql);
            }

            if(isset($_POST['btnLihat'])){
                $_SESSION['user'] = 1;
                $_SESSION['tempUser'] = $_POST['hiddenUser'];
            }

            if(isset($_POST['btnBack'])){
                $_SESSION['user'] = 0;
            }

            if(isset($_POST['btnGanti'])){
                $ganti = "";
                $temp = $_POST['hiddenSepatu'];
                $sql = "SELECT * FROM sepatu";
                $sepatu = $conn -> query($sql);
                while($row = $sepatu -> fetch_assoc()){
                    if($row['kode'] == $_POST['hiddenSepatu']){
                        if($row['status'] == "Sudah Di Kami")$ganti = "Sedang Diproses";
                        else if($row['status'] == "Sedang Diproses")$ganti = "Sudah Selesai";
                        else if($row['status'] == "Sudah Selesai")$ganti = "Sudah Selesai";
                        else if($row['status'] == "")$ganti = "Sudah Selesai";
                        else if($row['status'] == "Sedang Dikirim")$ganti = "Sudah Selesai";
                        $sql = "UPDATE sepatu SET status = '$ganti' WHERE kode = '$temp'";
                        $conn -> query($sql);
                    }
                }
            }
        ?>

        <form action="" method="post">
            <h1>Welcome to Admin Page <input type="submit" name="btnLogout" value="Log Out"></h1>
            <input type="submit" name="btnUser" value="Master User"> <input type="submit" name="btnCourier" value="Master Courier"> <input type="submit" name="btnHistory" value="History"><br>
            <?php if(isset($_SESSION['halamanAdmin']) && $_SESSION['halamanAdmin'] == 0){ ?>
                <?php if(isset($_SESSION['user']) && $_SESSION['user'] == 0){ ?>
                    <h2>List User</h2>
                    <table border = 1>
                        <tr>
                            <th>Nama</th>
                            <th>Username</th>
                            <th>No Telepon</th>
                            <th>Alamat</th>
                            <th>Kode Pos</th>
                            <th>Action</th>
                        </tr>
                        <?php 
                            $sql = "SELECT * FROM akunuser";
                            $user = $conn -> query($sql);
                            while($row = $user -> fetch_assoc()){?>
                                <tr>
                                    <td><?php echo $row['nama']; ?></td>
                                    <td><?php echo $row['username']; ?></td>
                                    <td><?php echo $row['notelp']; ?></td>
                                    <td><?php echo $row['alamat']; ?></td>
                                    <td><?php echo $row['kodepos']; ?></td>
                                    <td>
                                        <form action="" method="post">
                                            <input type="hidden" name="hiddenUser" value="<?php echo $row['nama']; ?>">
                                            <input type="submit" name="btnLihat" value="Lihat Sepatu">
                                        </form>
                                    </td>
                                </tr>
                        <?php } ?>
                    </table>
                <?php } ?>
                <?php if(isset($_SESSION['user']) && $_SESSION['user'] == 1){ ?>
                    <input type="submit" name="btnBack" value="Back"><br>
                    <h2>List Sepatu yang dimiliki <?php echo $_SESSION['tempUser']; ?></h2>
                    <table border = 1>
                        <tr>
                            <th>Kode</th>
                            <th>Merk</th>
                            <th>Jenis</th>
                            <th>Bahan</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        <?php 
                            $ctr = 1;
                            $sql = "SELECT * FROM sepatu";
                            $sepatu = $conn -> query($sql);
                            while($row = $sepatu -> fetch_assoc()){ 
                                if($row['pemilik'] == $_SESSION['tempUser']){
                        ?>
                                    <tr>
                                        <td><?php echo $row['kode']; ?></td>
                                        <td><?php echo $row['merk']; ?></td>
                                        <td><?php echo $row['jenis']; ?></td>
                                        <td><?php echo $row['bahan']; ?></td>
                                        <td><?php echo $row['status']; ?></td>
                                        <td>
                                            <form action="" method="post">
                                                <input type="hidden" name="hiddenSepatu" value="<?php echo $row['kode']; ?>">
                                                <?php if($row['status']!="Sudah Terkirim")?><input type="submit" name="btnGanti" value="Ganti Status">
                                            </form>
                                        </td>
                                    </tr>
                        <?php
                                    $ctr++;
                                }
                            }
                        ?>
                    </table>
                <?php } ?>
            <?php } ?>
            <?php if(isset($_SESSION['halamanAdmin']) && $_SESSION['halamanAdmin'] == 1){ ?>
                <h2>Registrasi Courier</h2>
                Nama : <input type="text" name="tbNama"><br>
                Username : <input type="text" name="tbUsername"><br>
                Password : <input type="text" name="tbPassword"><br>
                Alamat : <input type="text" name="tbAlamat"><br>
                Kode Pos : <input type="text" name="tbKodepos"><br>
                No Telepon : <input type="text" name="tbNotelp"><br>
                <input type="submit" name="btnSubmitRegis" value="Submit"><br><br>

                <h2>List Courier</h2>
                <table border=1>
                    <tr>
                        <th>Nama</th>
                        <th>Username</th>
                        <th>Alamat</th>
                        <th>Kode Pos</th>
                        <th>No Telepon</th>
                        <th>Action</th>
                    </tr>
                    <?php 
                        $sql = "SELECT * FROM courier";
                        $courier = $conn -> query($sql);
                        while($row = $courier -> fetch_assoc()){
                            ?>
                                <tr>
                                    <td><?php echo $row['nama'];?></td>
                                    <td><?php echo $row['username'];?></td>
                                    <td><?php echo $row['alamat'];?></td>
                                    <td><?php echo $row['kodepos'];?></td>
                                    <td><?php echo $row['notelp'];?></td>
                                    <td>
                                        <form action="" method="post">
                                            <input type="hidden" name="hiddenCourier" value="<?php echo $row['username']; ?>">
                                            <input type="submit" name="btnDelete" value="Delete">
                                        </form>
                                    </td>
                                </tr>
                            <?php
                        }
                    ?>
                </table>
            <?php } ?>
            <?php if(isset($_SESSION['halamanAdmin']) && $_SESSION['halamanAdmin'] == 2){ ?>
                <h2>History</h2>
                <table class='table'>
                    <tr>
                        <th>Date</th>
                        <th>Username</th>
                        <th>Nama</th>
                        <th>Courier</th>
                        <th>Kode Sepatu</th>
                        <th>Merk Sepatu</th>
                    </tr>
                    <?php 
                        $sql = "SELECT * FROM history";
                        $history = $conn -> query($sql);
                        while($row = $history -> fetch_assoc()){
                            ?>
                                <tr>
                                    <td><?php echo $row['tanggal'];?></td>
                                    <td><?php echo $row['username'];?></td>
                                    <td><?php echo $row['nama'];?></td>
                                    <td><?php echo $row['courier'];?></td>
                                    <td><?php echo $row['kodesepatu'];?></td>
                                    <td><?php echo $row['merk'];?></td>
                                </tr>
                            <?php
                        }
                    ?>
                </table>
            <?php } ?>
        </form>
    </body>
</html>
