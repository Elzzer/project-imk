<?php 
    session_start();
    $conn = mysqli_connect('localhost','root','','proyek_imk');

    if(isset($_POST['btnLogout'])){
        unset($_SESSION['tempUser']);
        unset($_SESSION['kirim']);
        unset($_SESSION['halamanCourier']);
        header("location:loginUser.php?");
    }

    if(isset($_POST['btnLihat'])){
        $_SESSION['kirim'] = 1;
        $_SESSION['tempUser'] = $_POST['hiddenUser'];
    }

    if(isset($_POST['btnBack'])){
        $_SESSION['kirim'] = 0;
    }

    if(isset($_POST['btnAmbil'])){
        $_SESSION['halamanCourier'] = 0;
    }

    if(isset($_POST['btnKirim'])){
        $_SESSION['halamanCourier'] = 1;
    }

    if(isset($_POST['btnAccept'])){
        $temp = $_SESSION['tempUser'];
        $sql = "SELECT * FROM sepatu";
        $sepatu = $conn -> query($sql);
        while($row = $sepatu -> fetch_assoc()){
            if($row['pemilik'] == $_SESSION['tempUser']){
                if($row['status'] == "Sudah Selesai"){
                    $sql = "UPDATE sepatu SET status = 'Sedang Dikirim' WHERE pemilik = '$temp'";
                    $conn -> query($sql);
                }
            }
        }
        $sql = "UPDATE akunuser SET kirim = 0 WHERE nama = '$temp'";
        $conn -> query($sql);

        $temp2 = $_SESSION['courierLogin'][0];
        $sql = "SELECT * FROM akunuser";
        $user = $conn -> query($sql);
        while($row = $user -> fetch_assoc()){
            if($row['nama'] == $_SESSION['tempUser']){
                $sql = "UPDATE akunuser SET courier = '$temp2' WHERE nama = '$temp'";
                $conn -> query($sql);
            }
        }
        $_SESSION['kirim'] = 0;
    }

    if(isset($_POST['btnAmbilBarang'])){
        $username = $_POST['hiddenAmbilUser'];
        $nama = $_POST['hiddenAmbilNama'];
        $sql = "UPDATE akunuser SET ambil = 0 WHERE username = '$username'";
        $conn -> query($sql);
        $sql = "SELECT * FROM sepatu";
        $sepatu = $conn -> query($sql);
        while($row = $sepatu -> fetch_assoc()){
            if($row['pemilik']==$nama){
                if($row['ambil']==1){
                    $kode = $row['kode'];
                    $sql = "UPDATE sepatu SET ambil = 0 WHERE kode = '$kode'";
                    $conn -> query($sql);
                    $sql = "UPDATE sepatu SET status = 'Sudah Di Kami' WHERE kode = '$kode'";
                    $conn -> query($sql);
                }
            }
        }
    }

    if(isset($_POST['btnKirim'])){
        $nama = "";
        $username = "";
        if(isset($_POST['hiddenKirimUser']))$username = $_POST['hiddenKirimUser'];
        if(isset($_POST['hiddenKirimNama']))$nama = $_POST['hiddenKirimNama'];

        $sql = "SELECT * FROM akunuser";
        $user = $conn -> query($sql);
        while($row = $user -> fetch_assoc()){
            if($row['username']==$username){
                $sql = "UPDATE akunuser SET courier = '' WHERE username = '$username'";
                $conn -> query($sql);
            }
        }

        $sql = "SELECT * FROM sepatu";
        $sepatu = $conn -> query($sql);
        while($row = $sepatu -> fetch_assoc()){
            if($row['pemilik']==$nama){
                $kode = $row['kode'];
                $sql = "UPDATE sepatu SET status = 'Sudah Terkirim' WHERE kode = '$kode'";
                $conn -> query($sql);
                $sql = "UPDATE sepatu SET kirim = 0 WHERE kode = '$kode'";
                $conn -> query($sql);
                $date = ((string)date("Y-m-d H:i:s"));
                $temp = $_SESSION['courierLogin'][0];
                $merk = $row['merk'];
                $sql = "INSERT INTO history(username,nama,courier,kodesepatu,merk) VALUES('$username','$nama','$temp','$kode','$merk')";
                $conn -> query($sql);

            }
        }
    }
?>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
    </head>
    <body>
    
    <form action="" method="post">
    <div class='row' style="background-color:rgb(223, 237, 255);">
        <div class='col-lg-3'></div>
        <div class='col-lg-6 text-center'>
            <p class="font-weight-light" style="font-size:50px;">Welcome to Courier Page, <?php echo $_SESSION['courierLogin'][2]; ?>
        
        </div>
        <div class='col-lg-1'></div>
        <div class='col-lg-2 text-center'>
            <button class="btn btn-info" style="width: 50%; margin-top: 10%;" type="submit" name="btnLogout">
                <img src="logouticon.png" style="margin-left: 2%; width:17%; height: 3%;">
                Log out
            </button>
        </div>
    </div>


    
    <br><center><input class="btn btn-info" type="submit" name="btnAmbil" value="Ambil Sepatu"> <input class="btn btn-info" type="submit" name="btnKirim" value="Kirim Sepatu"></center><br><br>
    <?php if(isset($_SESSION['halamanCourier']) && $_SESSION['halamanCourier'] == 0){ ?>
        <center><h1 class="font-weight-light">Pengambilan Barang</h1></center><br>
        <center><table border=1 class="table table-hover">
            <tr>
                <th><center>No</center></th>
                <th><center>Nama</center></th>
                <th><center>Alamat</center></th>
                <th><center>Kode Pos</center></th>
                <th><center>No Telepon</center></th>
                <th><center>Action</center></th>
            </tr>
            <?php
                    $ctr = 1;
                    $sql = "SELECT * FROM akunuser";
                    $user = $conn -> query($sql);
                    while($row = $user -> fetch_assoc()){
                        if($row['ambil']=='1'){
                ?>
                            <tr>
                                <td><center><?php echo $ctr; ?></center></td>
                                <td><center><?php echo $row['nama']; ?></center></td>
                                <td><center><?php echo $row['alamat']; ?></center></td>
                                <td><center><?php echo $row['kodepos']; ?></center></td>
                                <td><center><?php echo $row['notelp']; ?></center></td>
                                <td>
                                    <form action="" method="post">
                                        <center><input type="hidden" name="hiddenAmbilUser" value="<?php echo $row['username']; ?>">
                                        <input type="hidden" name="hiddenAmbilNama" value="<?php echo $row['nama']; ?>">
                                        <input type="submit" class="btn btn-default" style="background-color:rgb(248, 239, 233);" name="btnAmbilBarang" value="Ambil Barang"></center>
                                    </form>
                                </td>
                            </tr>
                <?php
                            $ctr++;
                        }
                    }
                ?>
        </table></center>
    <?php } ?>
    <?php if(isset($_SESSION['halamanCourier']) && $_SESSION['halamanCourier'] == 1){ ?>
        <?php if(isset($_SESSION['kirim']) && $_SESSION['kirim'] == 0){ ?>
        <div class='row'>
            <div class='col-lg-6 text-center'>
                <h1 class="font-weight-light">Pengiriman</h1><br>
                <center><table border=1 class="table table-hover" style="margin-left:1%;">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kode Pos</th>
                        <th>No Telepon</th>
                        <th>Action</th>
                    </tr>
                    <?php
                        $ctr = 1;
                        $sql = "SELECT * FROM akunuser";
                        $user = $conn -> query($sql);
                        while($row = $user -> fetch_assoc()){
                            if($row['courier']==$_SESSION['courierLogin'][0]){
                    ?>
                                <tr>
                                    <td><?php echo $ctr; ?></td>
                                    <td><?php echo $row['nama']; ?></td>
                                    <td><?php echo $row['alamat']; ?></td>
                                    <td><?php echo $row['kodepos']; ?></td>
                                    <td><?php echo $row['notelp']; ?></td>
                                    <td>
                                        <form action="" method="post">
                                            <input type="hidden" name="hiddenKirimNama" value="<?php echo $row['nama']; ?>">
                                            <input type="hidden" name="hiddenKirimUser" value="<?php echo $row['username']; ?>">
                                            <input type="submit" class="btn btn-default" style="background-color:rgb(248, 239, 233);" name="btnKirim" value="Kirim Barang">
                                        </form>
                                    </td>
                                </tr>
                    <?php
                                $ctr++;
                            }
                        }
                    ?>
                </table> </center> </div>

                <div class='col-lg-6 text-center'>
                <h1 class="font-weight-light">List User</h1><br>
                <center><table border=1 class="table table-hover" style="margin-left:-1%;">
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kode Pos</th>
                        <th>No Telepon</th>
                        <th>Action</th>
                    </tr>
                    <?php 
                        $ctr = 1;
                        $sql = "SELECT * FROM akunuser";
                        $user = $conn -> query($sql);
                        while($row = $user -> fetch_assoc()){
                            if($row['kirim']==1 && $row['courier']==""){
                    ?>
                                <tr>
                                    <td><?php echo $ctr; ?></td>
                                    <td><?php echo $row['nama']; ?></td>
                                    <td><?php echo $row['alamat']; ?></td>
                                    <td><?php echo $row['kodepos']; ?></td>
                                    <td><?php echo $row['notelp']; ?></td>
                                    <td>
                                        <form action="" method="post">
                                            <input type="hidden" name="hiddenUser" value="<?php echo $row['nama']; ?>">
                                            <input type="submit" class="btn btn-default" style="background-color:rgb(248, 239, 233);" name="btnLihat" value="Lihat Sepatu">
                                        </form>
                                    </td>
                                </tr>
                    <?php
                                $ctr++;
                            }
                        }
                    ?>
                </table></center></div>
            </div> 
        <?php } ?>
        <?php if(isset($_SESSION['kirim']) && $_SESSION['kirim'] == 1){ ?>
            <input type="submit" name="btnBack" value="Back"><br>
            
            <h2>List Sepatu yang dimiliki <?php echo $_SESSION['tempUser']; ?></h2>
            <table border = 1>
                <tr>
                    <th>Kode</th>
                    <th>Merk</th>
                    <th>Jenis</th>
                    <th>Bahan</th>
                    <th>Status</th>
                </tr>
                <?php 
                    $ctr = 1;
                    $sql = "SELECT * FROM sepatu";
                    $sepatu = $conn -> query($sql);
                    while($row = $sepatu -> fetch_assoc()){ 
                        if($row['pemilik'] == $_SESSION['tempUser'] && $row['kirim']==1){
                ?>
                            <tr>
                                <td><?php echo $row['kode']; ?></td>
                                <td><?php echo $row['merk']; ?></td>
                                <td><?php echo $row['jenis']; ?></td>
                                <td><?php echo $row['bahan']; ?></td>
                                <td><?php echo $row['status']; ?></td>
                            </tr>
                <?php
                            $ctr++;
                        }
                    }
                ?>
            </table><br>
            <input type="submit" name="btnAccept" value="Accept">
        <?php } ?>
    <?php } ?>
</form>


</body>
</html>