<?php
    session_start(); 
    $conn = mysqli_connect('localhost','root','','proyek_imk');
    if(isset($_POST['btnregister'])){
        header("location:registerUser.php?");
    }
    if(isset($_POST['btnlogin'])){
        if($_POST['txtusername']=="admin"&&$_POST['txtpassword']=="admin"){
            header("location:admin.php?");
            $_SESSION['halamanAdmin'] = 0;
            $_SESSION['user'] = 0;
        }
        else{
            $sql = "SELECT * FROM akunuser";
            $temp = $conn->query($sql);
            while($row = $temp->fetch_assoc()){
                if($row['username'] == $_POST['txtusername'] && $row['password'] == $_POST["txtpassword"]){
                    $_SESSION["userlogin"][0] = $row['username'];
                    $_SESSION["userlogin"][1] = $row['password'];
                    $_SESSION["userlogin"][2] = $row['nama'];
                    $_SESSION["userlogin"][3] = $row['alamat'];
                    $_SESSION["userlogin"][4] = $row['kodepos'];
                    $_SESSION["userlogin"][5] = $row['notelp'];
                    $_SESSION["userlogin"][6] = $row['status'];
                    header("location:service.php?");
                }
            }
            $sql = "SELECT * FROM courier";
            $temp = $conn->query($sql);
            while($row = $temp->fetch_assoc()){
                if($row['username'] == $_POST['txtusername'] && $row['password'] == $_POST["txtpassword"]){
                    $_SESSION["courierLogin"][0] = $row['username'];
                    $_SESSION["courierLogin"][1] = $row['password'];
                    $_SESSION["courierLogin"][2] = $row['nama'];
                    $_SESSION["courierLogin"][3] = $row['alamat'];
                    $_SESSION["courierLogin"][4] = $row['kodepos'];
                    $_SESSION["courierLogin"][5] = $row['notelp'];

                    $_SESSION['kirim'] = 0;
                    $_SESSION['halamanCourier'] = 0;
                    header("location:courier.php?");
                }
            }
        }
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        
        <style>
            #kotak{
                margin-left:380px;
                margin-top:200px;
                width:600px;
                height:300px;
                background-color:green;
                padding-left:170px;
                padding-top:5px;
            }
            body{
                background-image: url(sepatu.jpg);
                background-repeat: no-repeat;
                background-size:cover;
                
            }
        </style>
    </head>
    <body>
        <div class='row bg-dark' style='opacity:0.5;width:101%;'>
            <div class='col-lg-12' style='height:50px;'></div>
        </div>
        <br><br><br>
        <div class='row' style="width:100%;">
            <div class="col-md-4"></div>
            <div class="col-md-4" style='border:1px solid grey;box-shadow:2px 2px;opacity:0.8;background-image:linear-gradient(to bottom right, black, grey);'>
                <br>
                <center>
                <form action="" method="post">
                <div>
                    <h1 style='color:white;'>Login</h1>
                    <br>
                    <div class="form-group">    
                        <label for="username" style='color:white;'>Username</label><br>
                        <input class="col-sm-8" type="text" value="" name="txtusername" id="username"><br><br>
                    </div>
                    <div class="form-group">    
                        <label for="password" style='color:white;'>Password</label><br>
                        <input class="col-sm-8" type="text" value="" name="txtpassword" id="password"><br><br>
                    </div>
                        <input id='btn' class="btn btn-md" style='background-color:black;color:white;' type="submit" value="Login" name='btnlogin'>
                        <input id='btn' class="btn btn-md" style='background-color:black;color:white;' type="submit" value="Register" name='btnregister'>
                    </div>
                </form> 
                </center>
                <br><br>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div id='container'>
             
        </div>
        <div class='row bg-dark' style='opacity:0.5;width:101%;position:fixed;bottom:0;'>
            <div class='col-lg-12' style='height:50px;'></div>
        </div>
    </body>
</html>