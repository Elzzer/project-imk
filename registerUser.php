<?php
    session_start();
    $conn = mysqli_connect('localhost','root','','proyek_imk');
    if(isset($_POST['btnlogin'])){
        header("location:loginUser.php?");
    }

    if(isset($_POST['btnregister'])){
        if($_POST['txtpassword'] == $_POST['txtconfirm']){
            $sql = "select * from akunuser";
            $temp = $conn->query($sql);
            $cek = 0;
            while($row = $temp->fetch_assoc()){
                if($row['username'] == $_POST['txtusername'])$cek = 1;
            }
            if($cek == 0){
                $username = $_POST['txtusername']; $pass = $_POST['txtpassword'];
                $nama = $_POST['txtnama'];
                $alamat = $_POST['txtalamat'];
                $telp = $_POST['txttelp'];
                $pos = $_POST['txtpos'];
                $sql = "INSERT INTO akunuser VALUES('$nama','$username','$pass','$alamat','$telp','$pos','',0,0)";
                
                if($nama!=""&&$alamat!=""&&$pos!=""&&$telp!=""&&$username!=""&&$pass!=""&&$_POST['txtconfirm']!="")$conn->query($sql);
                else echo "<script>alert('Semua data harus diisi!!!')</script>";
            }
            else{
                echo "<script>alert('Username Telah Dipakai!!!')</script>";
            }
        }
        else echo "<script>alert('Confirm Password dan Password harus sama!!!')</script>";
    }
    
?>

<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        <style>
            
            body{
                background-image: url(shoes_old_boots_49231_1280x720.jpg);
                background-size: cover;
                background-repeat: no-repeat;
            }
        </style>
    </head>
    <body>
        <div class='row'>
            <div class='col-md-3'></div>
            <div class='col-md-6 bg-dark' style='color:white;'>
                
                <form action="" method="post">
                    <h1 class='text-center'> Register </h1>
                    <div class="form-group">    
                        <br><label for="nama" style="position:absolute; left:26%;">Nama</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txtnama" id="nama"></center>
                    </div>
                    <div class="form-group">    
                        <br><label for="username" style="position:absolute; left:26%;">Username</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txtusername" id="username"><label for="username" style='color:red; position:absolute;'> (Must Unique)</label></center>
                    </div>
                    <div class="form-group">    
                        <br><label for="password" style="position:absolute; left:26%;">Password</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txtpassword" id="password"></center>
                    </div>
                    <div class="form-group">    
                        <br><label for="confirm" style="position:absolute; left:26%;">Confirm Password</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txtconfirm" id="confirm"></center>
                    </div>
                    <div class="form-group">    
                        <br><label for="alamat" style="position:absolute; left:26%;">Alamat</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txtalamat" id="alamat"></center>
                    </div>
                    <div class="form-group">    
                        <br><label for="notelp" style="position:absolute; left:26%;">No Telepon</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txttelp" id="notelp"></center>
                    </div>
                    <div class="form-group">    
                        <br><label for="kodepos" style="position:absolute; left:26%;">Kode Pos</label><br>
                        <center><input style="width: 50%;" type="text" value="" name="txtpos" id="kodepos"></center>
                    </div>
                    <center><input class="btn btn-primary btn-md" type="submit" value="Register" name="btnregister">
                    <input class="btn btn-primary btn-md" type="submit" value="Login" name="btnlogin"></center>
                </form>
            </div>
            <div class='col-md-3'></div>
        </div>
        
    </body>
</html>
